[[!meta title="Iteration planning: November 18&ndash;December 02"]]
[[!meta date="2023-11-18 13:30:00 +0200"]]
[[!tag meeting]]

[[!toc levels=2]]

[previous meeting]: /blog/2023/10/07/planning/
[subplot-container-images]: https://gitlab.com/subplot/subplot-container-images
[subplot-web]: https://gitlab.com/subplot/subplot-web
[subplot]: https://gitlab.com/subplot/subplot

# Assessment of the iteration that has ended

The previous iteration was [[!ms 15]].

There were no actions from the [previous meeting][].

Subplot development has been quiet, mostly due to work, but some
progress is being made.

# Discussion

## The gitlab.com hosting situation

We reviewed [the billing
page](https://gitlab.com/groups/subplot/-/billings) for Subplot on
gitlab.com. It all looks OK.

We will review the billing page every iteration or two.

## Repository review

We reviewed issues (see below) and MRs. We made the decision
to keep the reference test MR open until the release process during
the next iteration.

We also checked in on CI pipelines for all the projects in the [Subplot
group](https://gitlab.com/subplot) on gitlab.com.

| Repository                   | Open issues | Closed issues | Open MRs | Merged MRs |              CI |
| :--------------------------- | ----------: | ------------: | -------: | ---------: | --------------: |
| [subplot-container-images][] |           1 |             7 |        0 |          6 |     OK (GitLab) |
| [subplot-web][]              |           0 |             5 |        0 |         93 |    OK (Ambient) |
| [subplot][]                  |          48 |           296 |        0 |        354 |     OK (GitLab) |

Extra branches: none.


## Recurring items issues

There are some issues tagged
[`recurring-agenda-item`](https://gitlab.com/groups/subplot/-/issues/?sort=created_asc&state=opened&label_name%5B%5D=recurring-agenda-item&first_page_size=20)
in the issue tracker, which we discuss in every iteration meeting.

- [[!subplotissue 119]] -- _Subplot is in not in Debian unstable_
  - Our plan is to wait until we can do `cargo update` using the Rust
    toolchain in Debian `testing` without having to adjust any of the
    dependencies to newer version, before we approach Debian about
    packaging Subplot.
  - We hope that an active Debian developer has interest in Subplot,
    as we neither in position to actively help in maintaining packages
    in Debian of Subplot and its dependencies. Time will tell if this
    happens.
  - Our MSRV should now be new enough that we no longer need to be
    careful about `cargo update`, but this needs to be confirmed.

# Goals

## Goal 4: new current goal

High level description of goal 4:

- It is plausible for someone to write a new Subplot for their project
  without support from Lars or Daniel.
- We shall aim for someone to do the above at or around Christmas 2023
- There shall be at least some general "Acceptance testing with
  Subplot" documentation as well as the expected usage material.

Things we might want issues for etc.

1. Installation document
2. Getting started.
   - Introduction to acceptance criteria
   - Tutorial / my-first-subplot-document
3. Step library documentation
4. General reference material for Python and Rust runners
5. Installable binary (`cargo install` is too much for non-Rust people)
   - Maybe a .deb?
   - Maybe just a downloadable binary and a `subplot-install.sh`
6. Tolerable HTML output
   1. Correct (not broken HTML as it is right now)
   2. Table of contents
   3. Some styling support (even if not a pretty style yet)
     - Maybe this involves templating the HTML somewhat?

Order of work:

* Engineering: 5, then 6.1 (maybe incl some of 6.3), then the rest of 6
* Documentation: 2, then 1 (once 5 is done), then 3, then 4.


([Related issues on gitlab](https://gitlab.com/subplot/subplot/-/issues?label_name%5B%5D=goal%3A%3A4))



# Plan for new iteration

## Issues

We did a review of new and changed issues. We made changes that Lars
forgot to make notes of.

We created [[!ms 16]] with the following issues:

- [[!issue 346]] -- _Our CI should fail if a library isn't sufficiently documented._
  - Lars will change `subplot libdocgen` to fail if a library doesn't
    have non-empty documentation for each step, with an option
    `--merciful` to allow that. He will then change `./check` to use
    `libdocgen` to produce formatted documentation for every library
    shipped with Subplot.
- [[!issue 344]] -- _The generated reference manual need to be
  published on <https://doc.subplot.tech>._
  - Lars will change Subplot CI to publish the generated library
    documentation on `https://doc.subplot.tech`.
  - This requires [[!issue 346]] to be done first.
- [[!issue 345]] -- _Add formatting markers (eg. classes) to all captures_
  - Lars will change how scenarios are typeset (not as a `<pre>`
    block, but as separate lines in a paragraph) and make captures are
    indicated with a `<span>` with a suitable class.
- [[!containerissue 8]] -- _Update baseline from bullseye to bookworm_
  - Daniel will change the `Dockerfile` to use `bookworm` instead of
    `bullseys` and fix anything that breaks.
- [[!issue 342]] -- _Create automated release artifacts_  
  [[!issue 305]] -- _Maybe create binary releases?
  - Lars and Daniel will collaborate on these.
- [[!issue 339]] -- _Doesn't warn about untyped captures in bindings
  files_
  - Daniel will fix this.

## Actions

No additional actions.

# Meeting participants

- Daniel Silverstone
- Lars Wirzenius
