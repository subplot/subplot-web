[[!meta title="Iteration planning: January 14&ndash;28"]]
[[!meta date="2023-01-14 15:50:00 +0000"]]
[[!tag meeting]]

# Notes

We decided to not follow the usual meeting structure and instead
only discussed how to refactor the Subplot code base, and specifically
its `Document` data type. Refactoring is needed because it's too much
work to make changes to Subplot. For example, replacing the Markdown
parser with another (which is something we're in need of doing).

After discussion we landed on a consensus that we'll want to break up
the current monolithic `Document` type into smaller types, and that
the first thing that needs doing is to stop using the `pandoc_ast`
crate's `Pandoc`, `Block`, and `Inline` types to represent the parsed
Markdown text.

We'll create a new abstraction for the parsed Markdown. The first
version will internally use `pandoc_ast`, and we'll use this to find
out exactly what interface the new type should offer to the rest of
Subplot. We'll then design and implement a new representation to fit
the interface we need. At that point, we will drop the reliance on
`pandoc_ast` and on Pandoc the program for parsing Markdown.

**DECISION:** We won't make a new release of Subplot until we can do
code generation (via `build.rs`) without Pandoc. This is to avoid
inflicting a run time dependency on Pandoc on those who merely want to
generate code from a subplot.

**DECISION:** Whatever we generate for our AST, our native output
format should be HTML, and that HTML should be reasonably importable
by Pandoc in order to generate other output formats. That way we don't
need Pandoc if the user wants only HTML output.


# Meeting participants

- Daniel Silverstone
- Lars Wirzenius
