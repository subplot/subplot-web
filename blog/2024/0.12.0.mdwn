[[!tag announcement]]
[[!meta date="2024-12-24 18:59:10 +0100"]]
[[!meta title="Subplot 0.12.0 released"]]

[Subplot]: https://subplot.tech/
[NEWS.md]: https://gitlab.com/subplot/subplot/-/blob/0.12.0/NEWS.md?ref_type=tags

The [Subplot][] project announces the 0.12.0 release of the tool for
documenting acceptance criteria and verifying them automatically. See
below for a summary of changes in this release, and [NEWS.md][] in the
git repository for release notes for all releases.

# Version 0.12.0, released 2024-12-24

- The binary has been moved to `subplot-bin` in order to reduce the
  dependency list in `subplot`.

- A new macro was added to `subplotlib` giving `subplotlib::codegen!()`
  currently very simple, but will gain functionality with newer
  releases. This reduces/removes the need for `subplot-build` in many
  cases and may result in its deprecation in future.

- Additional ergonomics were added to the `#[step]` attribute macro
  to deal with cases where the body of the function does not parse.
  This should improve IDE diagnostics at least a little.

- New `--log-level` argument to the `subplot` binary is available to
  make it easier to select simple logging levels rather than having
  to use the `SUBPLOT_LOG` environment variable.

- Logging formatter changed slightly. If you were relying on the
  exact format of log messages from the `subplot` tool then you may
  need to adjust your scripts. For example, instead of `INFO: ` and
  `ERROR: ` you will find `INFO` and `ERROR ` at the start of lines.

- New capture types `escapedword` and `escapedtext` added which process
  backslash escapes inside Subplot during code generation. Some bindings
  which previously did escape expansion in the step libraries have been
  updated to use this capture type instead.
