[[!meta title="Iteration planning note: October 24th"]]
[[!tag meeting]]
[[!meta date="2020-10-24 14:30"]]

# What has happened?

We finished [[!milestone 15]], completing [[!issue 83]], [[!issue 85]],
[[!issue 89]], [[!issue 90]], [[!issue 91]], and [[!issue 92]]. Lars made a
start on [[!issue 23]] and we need to discuss some more, so see later.

We closed [[!milestone 15]] and created [[!milestone 16]].

# Review

Outstanding MRs:

There were none.

Issues:

- [[!issue 95]] - Daniel added some clarification in a comment. We feel that
  such work would have to very configurable in order to not upset people.
  It's possible this could be a docgen option. We labelled it as such
- [[!issue 53]] - We discussed and agreed we could do annual code review (or
  more if we so choose) instead of holding this open.
- [[!issue 48]] - We discussed and decided that for now vendoring is OK and
  versioning the libraries would be a pain. We agreed to close this issue and
  made a note into it about how we can revisit when vendoring becomes awkward.
- [[!issue 23]] - We removed this from the milestone after discussing and
  deciding that for now there's not a lot we want to do on this topic.
- [[!issue 105]] - We labelled this performance and someday-maybe.
- [[!issue 20]] - Lars has worked out a good small first step, assigned himself
  to the issue, and tentatively put it into the milestone.
- [[!issue 107]] - We agreed on an implementation strategy and Lars assigned
  himself and tentatively added it to the milestone.
- [[!issue 108]] - We discussed that this is an interesting and valuable thing
  to do. Daniel suggested maybe we should do this as a subplot rather than as a
  yaml document. Added to the milestone, but not as yet assigned to a person.
- [[!issue 109]] - We decided there's nothing to do here for now.
- [[!issue 96]] - We need to change the error text, but also Lars wants the
  filename enough to assign it to himself and put it tentatively into the
  milestone.
- [[!issue 102]] - We investigated and found the dependencies for lmodern are
  likely via a `Recommends` from `texlive-base`. We put it tentatively into
  the milestone, assigned to Dan (in his absence).
- [[!issue 111]] - Assigned to Lars, tentatively put into milestone
- [[!issue 104]] - We agreed on a little more verbiage, and that it should go
  into `CONTRIBUTING.md`. Assigned to Lars and tentatively put into the
  milestone
- [[!issue 100]] - May as well satisfy the `curl|bash` people. Assigned to Lars
  and tentatively put into the milestone.
- [[!issue 22]] - We decided to thank Dan for his input and ask if he'd like to
  tackle this with mentoring when he's feeling a little more Rusty.
- [[!issue 101]] - This is hard, we're not working on it in this iteration.
- [[!issue 98]] - This is hard, ideally done as an additional pass on the AST
  to find and launch the converters, and then the typesetting pass can consume
  the results of those.
- [[!issue 97]] - Worth doing, but not right now.
- [[!issue 94]] - Daniel added helpwanted and mentor labels.
- [[!issue 93]] - Daniel was moderately excited and assigned to himself and put
  it into the milestone.
- [[!issue 74]] - Lars still tends to want this, so has assigned it to himself
  and put it into the milestone.
- [[!issue 58]] - Has not had any motion in 5 months, so we closed it.

At the start of review, there were 7 issues assigned to Lars, 1 to Dan, 1 to
Daniel, and 1 unassigned.

After discussion, Lars assigned [[!issue 108]] to himself, to give him roughly
a 50% load on his iteration timing.

# Discussion

We looked at Gitlab Iterations, sadly they're not available to us right now
because they need a Gitlab Group and we have the subplot repo under Lars' gitlab
namespace right now.

Lars' co-worker Ahmon has tentatively agreed to be interviewed. He's one of the
first people to actively use it and so asking for his opinions and experiences
would be helpful. He's prepared to be interviewed by both Lars and Daniel.
Ahmon's personal timezone puts his start-of-day at 15:00 to 16:00 UTC. Daniel's
least congested day is Thursday 5th November. Lars will speak with Ahmon to
discuss the best time to do this.

Subplot in Debian is still 'far future'.

# Actions

The only 'unusual' action coming out of this meeting was Lars needing to talk
with Ahmon to arrange an interview slot.
