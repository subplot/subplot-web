[[!meta title="Iteration planning note: May 9"]]
[[!tag meeting]]
[[!meta date="2020-05-09 18:59"]]


# What has happened

* We fixed all the issues we'd assigned to ourselves for the
  iteration, but didn't make much progress on the stretch goals. Lars
  feels he'd like to avoid stretch goals for future iterations.
  Emptying a column is a powerful positive motivator, and it's always
  OK to do more. Daniel agreed. We shall make it so.
  
# Discussion

* There were no outstanding merge requests.

* We reviewed the open issues that had been touched in the past week,
  and added comments with new thoughts. We assigned issues tentatively
  to the new iteration, then reviewed the planned iteration and
  dropped some issues.
  
* We used the GitLab suggestion tool, and found it worth using in the
  future.

* Lars would like to find someone who can improve the typesetting
  output (both HTML and PDF), but it's still not urgent.
  
* We think we've got the development and planning processes stable
  enough that we might onboard a third person into the project, if we
  happen to find someone who'd be interested. We'd be able to provide
  mentoring in Rust, if that's needed, but non-code contributions
  would also be most welcome.

# Actions

* Lars to write up these notes.
