[[!meta title="Iteration planning: March 12th &ndash; March 26"]]
[[!meta date="Sat, 12 Mar 2022 14:20:00 +0000"]]
[[!tag meeting]]

[[!toc levels=2]]

# Assessment of the iteration that has ended

[previous iteration]: /blog/2022/02/26/planning

The following issues were picked for the [previous iteration][]:

- [[!issue 214]] _Lacks example and docs for Rust step functions_
- [[!issue 271]] _`subplotlib` should be reviewed for inclusion of
  attributes such as `#[must_use]`_
- [[!issue 248]] _In Rust runcmd, write exit, stdout, and especially
  stderr upon failure_
- [[!issue 221]] _Command line tool has insufficient logging_

Additionally, we decided to blog about the FOSDEM talk, the new domain,
and the new Matrix room - this was done collaboratively between Lars
and Daniel and we made a [news](/blog/2022/02/28/news) posting.

We finished two issues:

- [[!issue 214]] _Lacks example and docs for Rust step functions_
- [[!issue 271]] _`subplotlib` should be reviewed for inclusion of
  attributes such as `#[must_use]`_

Also, left over from last iteration was [[!mr 250]] which Lars finished
reviewing and merged.

# Discussion and decisions

## [Recurring agenda items](https://gitlab.com/subplot/subplot/-/issues?sort=updated_desc&state=opened&label_name[]=recurring-agenda-item)

- [[!issue 119]] -- _Subplot is in not in Debian unstable_
- [[!issue 272]] -- _Migrate project to issue-based iteration meetings_
- [[!issue 273]] -- _Perform whole-codebase review and cleanup_

## Reaching out for feedback

We won't be reaching out for feedback until goal 3 is done. We didn't
open issue for this, to avoid naming people in public.

# Repository review

We reviewed the open issues, merge requests, and CI pipelines for all
the projects in the Subplot group on gitlab.com.

## The `subplot` repository

There were 57 open issues, of which 28 were not tagged
`someday-maybe`. We reviewed all 28 of these:

- [[!issue 141]] _subplotlib: Needs better test suite_ - we discussed this
  and decided it was good to close.
- [[!issue 221]] _Command line tool has insufficient logging_ we tentatively
  put this into the new iteration.
- [[!issue 248]] _In Rust runcmd, write exit, stdout, and especially
  stderr upon failure_
- [[!issue 262]] _Subplotlib's `ScenarioContext` should be able to be introspected
  for state_ we assigned to Daniel and put into the next iteration
- [[!issue 265]] _Tests (now) fail in my time zone_ was assigned to Daniel
  and put into the next iteration
- [[!issue 266]] _lib/files: could have a "file does not contain" step_ was
  assigned to Lars and put into the iteration
- [[!issue 267]] _lib/runcmd: lacks a way to redirect input from file_ had
  the _easy_, _helpwanted_, _feature_, and _mentor_ labels added
- [[!issue 276]] _Lacks an HTTP client step library_ we discussed, agreed on
  a rough shape for integration, but also agreed to defer this for now.

There were open, old branches:

- `subplot-rust` -- kept until the current goal is finished
- `docgen-cmark` -- kept until the next goal is finished

CI pipelines are OK.

## The `subplot-web` repository

There were no open issues, no open merge requests, no extra branches,
and no CI enabled.

## The `subplot-container-images` repository

There was one open issue.

- [subplot-container-images
  #2](https://gitlab.com/subplot/subplot-container-images/-/issues/2)
  -- _There is no Docker image for using Subplot_
  - we're not going to work on this until we stop making breaking
    changes

There were no open merge requests, no extra branches, and the latest
run of CI was successful. (That's the run that was automatically
triggered yesterday.)

# New iteration

## Current goal (not changed this iteration)

The current development goal is:

> Subplot provides a set of libraries with identical capabilities in
> each of the supported languages. Python remains a supported
> language. Rust is promoted to supported-language status. Subplot
> will be tested with all supported languages. In addition, any
> quality of life improvements which can be done shall be done. This
> goal will be considered complete when a release of Subplot has been
> made with the unified language handling support complete.

This is represented as label
[`goal:2`](https://gitlab.com/subplot/subplot/-/issues?sort=created_asc&state=opened&label_name[]=goal::2)
in the GitLab.com issue tracker.

## Issues for this iteration

We collect issues for this iteration in [[!milestone 46]]. We decided
to not make a release.

Lars intends to work on:

- [[!issue 266]] -- _lib/files: could have a "file foo does not contain" step_
- [[!issue 221]] -- _Command line tool has insufficient logging_

Daniel intends to work on:

- [[!issue 265]] -- _Tests (now) fail in my time zone_
- [[!issue 262]] -- _Subplotlib's `ScenarioContext` should be able to be introspected for state_
- [[!issue 248]] -- _In Rust runcmd, write exit, stdout and especially stderr upon failure_

# Actions

# Meeting participants

- Daniel Silverstone
- Lars Wirzenius
