[[!meta title="Iteration planning: July 31 &ndash; August 13"]]
[[!meta date="2022-07-30 13:30:00 +0100"]]
[[!tag meeting]]

[[!toc levels=2]]

# Actions from last time

- Daniel to draft a response to Hillel Wayne. Lars will review.
  **DONE** Email reply was sent, Saturday July 30th.
- Daniel to research how to have dependencies only for binaries
  produced by a crate, but not the library part of the crate.
  _Some research done, an issue is to be opened on Cargo_
- Lars to upgrade his `roadmap` crate to use `clap` version 3, instead
  of `structopt`. **DONE as 0.4.4**

# Assessment of the iteration that has ended

We extended, by mutual agreement on Matrix, this iteration was extended
by two weeks.

We had chosen the following issues for this iteration:

- [[!webissue 3]] -- _Document how we do meetings_ - **DONE**
- [[!subplotissue 283]] -- _YAML metadata is not in its own file_
  - Lars has made some progress
- [[!subplotissue 286]] -- _Tests sometimes fail_
  - Lars tried to reproduce, didn't succeed, **closed**.
- [[!webissue 4]] -- _Review FAQ_
  - Daniel has made little progress on this, to be carried.
- [[!subplotissue 289]] -- _Make release 0.4.2_
  - Lars made the release, then made another release. Yay.
- [[!subplotissue 287]] -- _Needs documentation for Rust step functions
  for implementing contexts_
  - Daniel has made no progress here, to be carried.

We managed to complete three of these issues, and make progress on the
non-issue based actions from last meeting. This is good.

We chose to carry all three open issues to the next iteration.

# Discussion

## Lars' progress on [[!subplotissue 283]]

We have two metadata structs at the moment. Rather than trying to find a unified version of these and then refactoring everything to use that, etc. etc. Lars realised there's a reason why we have two. They serve different purposes. One represents the data that is serialised as YAML - the other represents the processed version of the metadata where things like bindings and implementations have been loaded in. Effectively raw metadata and processed metadata.

So today, Lars started to write a struct (`MetadataFile`) which is raw metadata. And then added functions to the `Document` struct impls, to deal with these. Inherently this works OK, but we cannot use these for `subplot-filter` as that works with pandoc json. We _think_ that Lars is the only user of `subplot-filter` and so we agreed to drop that binary from the crate, thereby simplifying matters for this refactoring.

## Check through licence terms we accept and confirm we're okay with them all.

Assuming all are good (e.g. the unicode licence we had to accept recently) we
should write a statement about them in our README or some similar document.

We reviewed the [SPDX page](https://spdx.org/licenses/Unicode-DFS-2016.html)
for the Unicode license agreement and concluded together that we are:

1. comfortable that it does not cause us problems
2. comfortable that it will not cause problems for our users

As such, we see no need to worry about this licence.

We chose to be lazy and not update our README at this time.

## EuroRust in Octber in Berlin

Daniel will be unavailable, but Lars in theory could attend, either in person or remotely.

We decided that Lars should give a talk, especially given we can always back down later if something bad happens.

We think it might mix into the Rust patterns and idioms, or the Rust case studies threads for EuroRust.

If we write a talk about how people should use Subplot; then we could use that thinking to also further the goal of defining the Subplot book.

## The gitlab.com hosting situation

We reviewed [the billing
page](https://gitlab.com/groups/subplot/-/billings) for Subplot on
gitlab.com. It all looks OK. We're over the limit on seats, but it
seems we can increase the seats for free.

We will review the billing page every iteration or two.

## Repository review

We reviewed issues updated since the previous
meeting, merge requests, and CI pipelines for all the projects in the
[Subplot group](https://gitlab.com/subplot) on gitlab.com.

### [Subplot Container Images](https://gitlab.com/subplot/subplot-container-images)

- Open issues: 1
- Merge requests: 0
- Additional branches: 0
- CI: OK, ran on Friday

### [subplot-web](https://gitlab.com/subplot/subplot-web)

- Open issues: 1
- Merge requests: 1
- Additional branches: 0
- CI: Ick, OK.

### [subplot](https://gitlab.com/subplot/subplot)

- Open issues: 47, we reviewed all non-someday-maybe non-recurring issues:
  - [[!subplotissue 287]] we added the documentation and subplotlib labels
  - [[!subplotissue 290]] we assigned to Daniel and put into the iteration.
- Merge requests: 0
- Additional branches:
  - `docgen-cmark`, Lars is keeping this around for reference for
    future changes
- CI: OK

## Recurring items issues

There are some issues tagged
[`recurring-agenda-item`](https://gitlab.com/groups/subplot/-/issues/?sort=created_asc&state=opened&label_name%5B%5D=recurring-agenda-item&first_page_size=20)
in the issue tracker, which we discuss in every iteration meeting.

- [[!subplotissue 119]] -- _Subplot is in not in Debian unstable_
  - we will work on reducing Subplot dependencies that aren't already
    in Debian
- [[!subplotissue 272]] -- _Migrate project to issue-based iteration
  meetings_
  - we will try a "merge request based approach" instead, for a few
    iterations
  - Lars has tried this in the Obnam project and it has worked OK
  - Daniel agrees that so far this feels better.
  - We will try for another iteration or two, before deciding.
- [[!subplotissue 273]] -- _Perform whole-codebase review and cleanup_
  - we are not up to doing this yet

# Goals

## Goal 3: Current goal

Subplot is altered to treat a YAML document as the root of any subplot
document, permitting multiple markdown files to be joined into a
single scenario set or output document. We remove our dependency on
Pandoc’s AST for internal document representation, and support a
better range of document metadata as a result. We will use marked data
types where possible to permit better provenance information in error
and warning messages; and we will strive to reenable the reference
tests such that we can make a statement to the effect that post goal
3, we do not intend to make further breaking changes to the input
formats or primary behaviours of Subplot.

([Related issues on gitlab](https://gitlab.com/subplot/subplot/-/issues?label_name%5B%5D=goal%3A%3A3))

## Goal 4: Planned

This goal is very likely to change. This is merely an initial sketch.

We'll improve documentation and generally aim at making Subplot more
plausibly usable by people outside of the Subplot project.

([Related issues on gitlab](https://gitlab.com/subplot/subplot/-/issues?label_name%5B%5D=goal%3A%3A4))

# Plan for new iteration

We intend to work on the following issues:

- [[!subplotissue 290]] -- _Cargo.toml: homepage URL is wrong_
- [[!webissue 4]] -- _Review FAQ_
- [[!subplotissue 283]] -- _YAML metadata is not in its own file_
- [[!subplotissue 287]] -- _Needs documentation for Rust step
  functions for implementing contexts_

## Actions

These are in addition to work on issues chosen for this iteration.

- Daniel to open an issue with Cargo to discuss binary-only dependencies.
- Lars to prepare an abstract and outline for a talk for EuroRust for us to
  review and make a decision about whether or not to submit at the next meeting.
- Lars to use his new tool to produce a list of dependencies which Subplot
  has, which are not currently met by the packages in Debian.

# Meeting participants

- Daniel Silverstone
- Lars Wirzenius
